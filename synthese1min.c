#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define FE 44100
#define DUREE 5
#define N 1024

/* output */
static char *RAW_OUT = "tmp-out.raw";
static char *FILE_OUT = "out.wav";

FILE *
sound_file_open_write (void)
{
 return fopen (RAW_OUT, "wb");
}

void
sound_file_close_write (FILE *fp)
{
  char cmd[256];
  fclose (fp);
  //snprintf(cmd, 256, "sox -c 2 -r %d -e signed-integer -b 16 %s %s", (int)FE, RAW_OUT, FILE_OUT);
  snprintf(cmd, 256, "sox -c 1 -r %d -e signed-integer -b 16 %s %s", (int)FE, RAW_OUT, FILE_OUT);
  system (cmd);
}

void
sound_file_write (double *s, FILE *fp)
{
  int i;
  short tmp[N];
  for (i=0; i<N; i++)
    {
      tmp[i] = (short)(s[i]*32768);
    }
  fwrite (tmp, sizeof(short), N, fp);
}
double freq_modif(double freq,double i) {
    return freq +(i*10);
}
//c'est la fonction qui décale
//mais il faut la faire en fonction de la fréquence
double delta_phi() {

}
double indice_de_modulation(double t,double max,int mode) {
  double I;
  for(int i =0; i<t; i++) {
    if(t == 0) {
      I = max;
    } 
    if(t>0 && t<=5) {
      I ++;
    }
    if(t>5 && t<=10) {
      I--;
    }
    if(t>10 && t<=20) {
      I+=10;;
    }
    if(t>20) {
      I-=10;;
    }
  }
  return I;
 }

//Son(double amp1,double amp2,double freq1, double freq2)

int
main (int argc, char *argv[])
{
  int i;
  FILE *output;
  double s[N];

  if (argc != 1)
    {
      printf ("usage: %s\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  output = sound_file_open_write ();
    double amp=0.5;
    double phi = 3.1416;//phase entre 0 et 2*pi
    double freq=440;//entre 22050 //440 La
    double freq2=220;
    double * I = malloc (sizeof(double)*60*FE/N);
    double * A = malloc (sizeof(double)*60*FE/N);
    for(int j =0; j<60*FE/N;j++ ){
      I[j] = indice_de_modulation(j,5,1);
      A[j] = indice_de_modulation(j,0.5,0);
        double delta_phi = 2*3.1416 *freq*N*1.0/FE *(double)j;
        
      //synthèse additif
        for (i=0; i<N; i++) {
            

            //guitare électrique très très bugguée
            //s[i] = sin(2.0*3.1416*117*(double)i/(double)FE)+sin(2.0*3.1416*233*(double)i/(double)FE)+sin(2.0*3.1416*466*(double)i/(double)FE); // ECHANTILLONS

            //language alien amp =0.1 freq =65 freq2=131
            //s[i] = amp * sin(delta_phi+2*3.1416*1.0/freq*(double)i +I[j] * sin(delta_phi+2*3.1416*1.0/freq2*(double)i)); // ECHANTILLONS
            
            //
            //s[i] = amp * sin(2*3.1416*1.0/freq*(double)i) *I[j] * sin(2*3.1416*1.0/freq2*(double)i); // ECHANTILLONS
            
            s[i] =  0.5 * sin(delta_phi+2*3.1416*1.0/freq*(double)i +I[j] * sin(2*3.1416*1.0/freq2*(double)i)); // ECHANTILLONS

        
        }
        sound_file_write (s, output);
    }
  
  sound_file_close_write (output);
  exit (EXIT_SUCCESS);
}
