#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <sndfile.h>

#include <math.h>
#include <complex.h>
#include <fftw3.h>
#include <stdbool.h>

/* Taille (cad duree) des buffers */
#define	FRAME_SIZE 1024

#define M_PI       3.14159265358979323846 //ajout car il y un problème 

/* Avancement */
#define N1 512
#define N2 512

typedef fftw_complex spectrum[FRAME_SIZE];

static fftw_plan plan = NULL;
static fftw_plan iplan = NULL;


static void
print_usage (char *progname)
{	printf ("\nUsage : %s <input file> <output file>\n", progname) ;
	puts ("\n"
		) ;
} 


// IFFT
void
ifft_init (spectrum in, spectrum out)
{
  iplan = fftw_plan_dft_1d (FRAME_SIZE, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
}

void
ifft_exit (void)
{
  fftw_destroy_plan (iplan);
}

void
ifft_process (void)
{
  fftw_execute (iplan);
}

// FFT
void
fft_init (spectrum in, spectrum out)
{
  plan = fftw_plan_dft_1d (FRAME_SIZE, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
}

void
fft_exit (void)
{
  fftw_destroy_plan (plan);
}

void
fft_process (void)
{
  fftw_execute (plan);
}

void
polar(spectrum S, double *amplitude, double *phase) //obtenir amplitude et phase à partir du complexe
{
  /* a completer */
  for(int i = 0; i<FRAME_SIZE; i++) {
    amplitude[i] = cabs(S[i]);//module phi
    phase[i] = carg(S[i]);//argument teta
  }
}

void
comp(spectrum S, double *amplitude, double *phase) //refaire le complexe à partir de l'amplitude et la phase
{
  /* a completer */
  for(int i = 0; i<FRAME_SIZE; i++) {
    S[i] = amplitude[i] * cexp(I*phase[i]);
  }
}

//pour le OLA, fenêtre de Hann = fenêtre triangulair

void hann(double * s)
{
  int i;
  double w;
  for (i=0; i<FRAME_SIZE; i++) {
      w = 0.5 * (1 - cos (2*M_PI*i*1./FRAME_SIZE));
      s[i] *= w;
    }
}

bool verif(spectrum S) {
	int tot = 0;
	for (int i = 1; i<FRAME_SIZE/2;i++) {

		//printf("%d/%d;",S[FRAME_SIZE - i], carg(S[i]));
		if(S[FRAME_SIZE - i] == carg(S[i])) {
			tot ++;
		}
	}
	
	if( tot == FRAME_SIZE/2)
		return true; 
	return false;
}

bool modif(spectrum S) {
	for (int i = 1; i<FRAME_SIZE/2;i++) {
		S[FRAME_SIZE - i] = (fftw_complex)carg(S[i]);
		//printf("%d;",S[FRAME_SIZE - i]);
	}
}

//double * 



int
main (int argc, char * argv [])
{	char 		*progname, *infilename, *outfilename ;
	SNDFILE	 	*infile = NULL ;
	SNDFILE		*outfile = NULL ;
	SF_INFO	 	sfinfo ;
	SF_INFO	 	sfinfo_out ;

	progname = strrchr (argv [0], '/') ;
	progname = progname ? progname + 1 : argv [0] ;

	if (argc != 3)
	{	print_usage (progname) ;
		return 1 ;
		} ;

	infilename = argv [1] ;
	outfilename = argv [2] ;
	
	if (strcmp (infilename, outfilename) == 0)
	{	printf ("Error : Input and output filenames are the same.\n\n") ;
		print_usage (progname) ;
		return 1 ;
		} ;


	if ((infile = sf_open (infilename, SFM_READ, &sfinfo)) == NULL)
	{	printf ("Not able to open input file %s.\n", infilename) ;
		puts (sf_strerror (NULL)) ;
		return 1 ;
	} ;

	printf("Processing : %s\n",infilename);
	
	/* verify sampling rate */
	printf("Sampling rate : %d\n",sfinfo.samplerate);
	printf("Channels : %d\n",sfinfo.channels);

	if (sfinfo.samplerate != 44100)
	  {
	    printf("Error : processing only 44100 Hz files \n");
	    exit(EXIT_FAILURE);
	  }

	if (sfinfo.channels != 1)
	  {
	    printf("Error : processing only MONO files \n");
	    exit(EXIT_FAILURE);
	  }
	

	if ((sfinfo.format | SF_FORMAT_PCM_16)== 0)
	{
	    printf("Error : processing only 16bits files \n");
	    exit(EXIT_FAILURE);
	  }
	  
	sfinfo_out.samplerate = sfinfo.samplerate;
	sfinfo_out.channels = sfinfo.channels;
	sfinfo_out.format = sfinfo.format;
		
	if ((outfile = sf_open (outfilename, SFM_WRITE, &sfinfo_out)) == NULL)
	{	printf ("Not able to open output file %s.\n", outfilename) ;
		puts (sf_strerror (NULL)) ;
		return 1 ;
	} ;
	

	// variables complexes
	fftw_complex in[FRAME_SIZE];//S1
	fftw_complex out[FRAME_SIZE];//S2
	
	fft_init(in, out);//transf fourier 
	ifft_init(out, in);//transf fourier inverse

	// Samples
	int l = (int)sfinfo.frames;
	int length_in = (int)sfinfo.frames+FRAME_SIZE+FRAME_SIZE-(l%N1);
	double *samples_in = malloc(sizeof(double)*length_in);
	assert(samples_in);

	int length_out = FRAME_SIZE+(int)ceil(length_in);
	double *samples_out = malloc(sizeof(double)*length_out);
	assert(samples_out);
	
	int i;
	for (i=0; i< length_in; i++)
	  samples_in[i] = 0.0;
	for (i=0; i< length_out; i++)
	  samples_out[i] = 0.0;
	
	// Wav read
	int readcount = 0;
	readcount = sf_readf_double(infile, samples_in+FRAME_SIZE, sfinfo.frames);
	if (readcount < sfinfo.frames)
	  printf("problème lecture %d %d\n", readcount, (int)sfinfo.frames);
	
	//
	int pin = 512;
	int pout = 0;
	int pend = length_in-FRAME_SIZE;
	double s[FRAME_SIZE];//trame
	int j = 0;
	double norm = 1.0*FRAME_SIZE;
	double amplitude[FRAME_SIZE];
	double phase[FRAME_SIZE];

	
	while (pin < pend)
	  {
		
	    for (j= 0; j < FRAME_SIZE;j++)
	      s[j] = samples_in[pin+j];
		
		//OLA
		hann(s);
		//on met le spectre dans l'entré
		for (int i=0; i<FRAME_SIZE; i++) {
    	  in[i] = s[i];
      	}
	      
		// FFT
		fftw_execute(plan);
		//recup amplitude et phase
		polar(out,amplitude,phase);

		
	    // amplitude avec prise en compte du symétrique
		// for(int i = 0; i < FRAME_SIZE/2; i++) {
        // 	if((i*44100)/FRAME_SIZE >1000) {
		// 		amplitude[i] = 0;
		// 	}

     	// }
		// for (int i = 1; i < FRAME_SIZE/2; i++)
		// {
		// 	if((i*44100)/FRAME_SIZE >1000) {
		// 		amplitude[FRAME_SIZE - i] = 0;
		// 	}
		// }


		/*amplitude sans prise en compte du symétrique*/
		for(int i = 0; i < FRAME_SIZE; i++) {
        	if((i*44100)/FRAME_SIZE >2000) {
				amplitude[i] = 0;
			}
     	}

		

	    // phase robot met la phase à 0 pour casser la phase
		// for (int i = 0; i < FRAME_SIZE; i++) {
        // 	//if(i == 0)
        //    		phase[i] = 0;
        // 	//else
        //   	//	break;
      	// }


	    // Processing
		//hann(out);
	    /* Reconstruction */
	    comp(out,amplitude,phase);

	    // IFFT out ds in
		fftw_execute(iplan);

		// if(!verif(s)){
		// 	modif(s);
		// 	if(!verif(s)){
		// 		printf("prout");
		// 	}
		// }

		
	    
		for (i=0;i<FRAME_SIZE;i++) {
    	  s[i] = creal(in[i])/FRAME_SIZE;
      	}
		// if(!verif(s)){
		// 	modif(s);
		// 	if(!verif(s)){
		// 		printf("prout");
		// 	}
		// }
	    for (j= 0; j < FRAME_SIZE;j++) {
	      samples_out[pout+j] += s[j];
		}
		//OLA ici aussi si la transformation est non-linéaire
		//hann(samples_out);
	    pin = pin + N1;
	    pout = pout+N2;
	  }

	// Save and normalize
	double max_samples = 0.0;
	
	for (i=0; i< length_out;i++)
	  {
	    if (fabs(samples_out[i])>max_samples)
	      max_samples = fabs(samples_out[i]);
	  }

	printf("normalize : %f\n", max_samples);
		
	for (i=0; i< length_out;i++)
	  samples_out[i] = samples_out[i]/max_samples;
	
	int writecount = 0;
	writecount = sf_writef_double(outfile, samples_out+FRAME_SIZE, length_out-FRAME_SIZE);
	if (writecount < length_out-FRAME_SIZE)
	  printf("problème ecriture %d %d\n", writecount, length_out-FRAME_SIZE);
	
	/* exit */
	fft_exit();
	ifft_exit();
	free(samples_in);
	free(samples_out);
	sf_close (infile) ;
	sf_close (outfile) ;
	
	return EXIT_SUCCESS ;
} /* main */

