#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>
#include "gnuplot_i.h"

#define M_PI       3.14159265358979323846 //ajout car il y un problème 

#define FE 44100
#define N 1024

typedef double frame[N];
typedef fftw_complex spectre[N];

/* input */
static char *RAW_IN = "tmp-in.raw";
static double cache_in[N/2];

/* output */
static char *RAW_OUT = "./result/tmp-out.raw";
static char *FILE_OUT = "./result/out.wav";
static double cache_out[N/2];

/* ctrl fftw */
static fftw_plan plan1 = NULL;
static fftw_plan plan2 = NULL;

/* ctrl gnuplot*/
static gnuplot_ctrl *h;

FILE *
sound_file_open_read (char *file_in)
{
  char cmd[256];
  snprintf(cmd, 256, "sox %s -c 1 -r %d -b 16 -e signed-integer %s", file_in, (int)FE, RAW_IN);
  system (cmd);  
  return fopen(RAW_IN, "rb");
}

void
sound_file_close_read (FILE *fp)
{
  fclose (fp);
}

int
sound_file_read (FILE *fp, frame s)
{
  int i, result;
  //découpe en frame
  static short tmp[N/2];
  for (i=0; i<N/2; i++)
    {
      s[i] = cache_in[i];
    }

  bzero (tmp, N/2*sizeof(short));
  
  result = fread (tmp, sizeof(short), N/2, fp);
  
  for (i=0; i<N/2; i++)
    {
      s[N/2+i] = tmp[i] / 32768.0;
      cache_in[i] = s[N/2+i];
    }
  return (result == N/2);
}

FILE *
sound_file_open_write (void)
{
 return fopen (RAW_OUT, "wb");
}

void
sound_file_close_write (FILE *fp)
{
  char cmd[256];
  fclose (fp);
  snprintf(cmd, 256, "sox -c 1 -r %d -b 16 -e signed-integer %s %s", (int)FE, RAW_OUT, FILE_OUT);
  system (cmd);
}

void
sound_file_write (frame s, FILE *fp)
{
  int i;
  static short tmp[N/2];
  for (i=0; i<N/2; i++)
    {
      double v = (s[i] + cache_out[i])/2;
      short t = (v < -1) ? -32768 : (v > 1) ? 32767 : (short) (v * 32767);
      cache_out[i] = s[N/2+i];
      tmp[i] = t;
    }
  fwrite (tmp, sizeof(short), N/2, fp);
}

/* fftw_plan fftw_plan_dft_1d(int n, fftw_complex *in, fftw_complex *out,
                           int sign, unsigned flags);*/
void
fft_init (spectre in, spectre spec)
{
  plan1 = fftw_plan_dft_1d(N,in,spec,FFTW_FORWARD,FFTW_ESTIMATE);
  plan2 = fftw_plan_dft_1d(N,in,spec,FFTW_BACKWARD,FFTW_ESTIMATE);
}

void
fft_exit (void)
{
  fftw_destroy_plan (plan1);
  fftw_destroy_plan (plan2);
}

void
fft (void)
{
 /* a completer */
  fftw_execute(plan1);
}

void
ifft (void)
{
  /* a completer */
  fftw_execute(plan2);
}

void
polar(spectre S, double *amplitude, double *phase) //obtenir amplitude et phase à partir du complexe
{
  /* a completer */
  for(int i = 0; i<N; i++) {
    amplitude[i] = cabs(S[i]);//module phi
    phase[i] = carg(S[i]);//argument teta
  }
}

void
comp(spectre S, double *amplitude, double *phase) //refaire le complexe à partir de l'amplitude et la phase
{
  /* a completer */
  for(int i = 0; i<N; i++) {
    S[i] = amplitude[i] * cexp(I*phase[i]);
  }
}
//pour le OLA, fenêtre de Hann
void hann(frame s)
{
  int i;
  double w;
  for (i=0; i<N; i++) {
      w = 0.5 * (1 - cos (2*M_PI*i*1./N));
      s[i] *= w;
    }
}
double * passehaut() {
  double * fhaut;
  for (int i = 0; i<N ; i++) {
    if(i>N/2)
      fhaut[i] = 1;
    else
      fhaut[i] = 0;
  }
  return fhaut;
}

int
main (int argc, char *argv[])
{
  int i, f_coup;
  FILE *input, *output;
  frame s, amplitude, phase;//s trame , tableau amplitude / phase
  spectre S1, S2;//données complexes 

  if (argc != 2)
    {
      printf ("usage: %s <soundfile>\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  fft_init(S1, S2);//formalisme de la trrans de fourrier

  h = gnuplot_init();//afficher initilisation
  gnuplot_setstyle(h, "lines");

  input = sound_file_open_read (argv[1]);
  output = sound_file_open_write ();

  bzero (cache_in, N/2*sizeof(double));//met des 0 
  bzero (cache_out, N/2*sizeof(double));

  while (sound_file_read (input, s))//tant c'est pas fini
    {
      //hann(s);
      for (int i=0; i<N; i++) {
    	  S1[i] = s[i];
      }
      
      fft();
      
      polar(S2, amplitude, phase);
      // passe bande test BEURK
      // double coef_a[4] ={0.81,0.82,-0.16,-1.31};
      // double coef_b[4] ={0.92,0.84,0.14,0.05};

      // coupe-haut OK
      // double coef_a[4] ={0.81,0.82,0.76,0.64};
      // double coef_b[4] ={0.92,0.84,0.74,0.95};

      // coupe-bas OK
      //double coef_a[4] ={-1.31,0.82,-0.16,0.64};
      //double coef_b[4] ={0.12,0.04,0.14,0.05};

      // coupe-bas ++
      double coef_a[4] ={-1.31,-0.82,-0.16,-0.64};
      double coef_b[4] ={-0.12,-0.04,-0.14,-0.05};

      // fct hard
      /*
      for(int i = 0; i < N; i++) {
        if (i==0) {
          S1[i] = S1[i];
        }
        else {
          for(int j = 0; j<4; j++) {
            S1[i] = s[i-1]* coef_b[j] + S1[i-1] * coef_a[j];
          }   
        }
      }
      */
     //double fhaut[N] = passehaut();

      double fhaut[N];
      for (int i = 0; i<N ; i++) {
        if(i>N/2)
          fhaut[i] = 0;
        else
          fhaut[i] = 1;
      }
      for(int i = 0; i < N; i++) {
        S1[i]  = fhaut[i] * s[i];
     }


     
      // effet robot -> casser la phase algo, 0-phase en milieu de phase
      // for (int i = 0; i < N; i++) {
      //   if(i == 0)
      //      phase[i] = 0;
      //   else
      //     break;
      // }
      
     
      //gnuplot_resetplot(h);
      //gnuplot_plot_x(h , phase, N, "phase");
      //gnuplot_plot_x(h , amplitude, N, "amplitude spectrum");
      //usleep(100000);

      comp(S2, amplitude, phase);
      
      ifft();

      for (i=0;i<N;i++) {
        //printf("phase[%d] = %d\n",i,phase[i]);
    	  s[i] = creal(S1[i]);
      }

      sound_file_write (s, output);
    }

  /* exit */
  sound_file_close_write (output);
  sound_file_close_read (input);
  fft_exit ();
  exit (EXIT_SUCCESS);
}

