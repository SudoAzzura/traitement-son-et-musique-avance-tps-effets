#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define FE 44100
#define DUREE 5
#define N DUREE*FE

/* output */
static char *RAW_OUT = "tmp-out.raw";
static char *FILE_OUT = "out.wav";

FILE *
sound_file_open_write (void)
{
 return fopen (RAW_OUT, "wb");
}

void
sound_file_close_write (FILE *fp)
{
  char cmd[256];
  fclose (fp);
  //snprintf(cmd, 256, "sox -c 2 -r %d -e signed-integer -b 16 %s %s", (int)FE, RAW_OUT, FILE_OUT);
  snprintf(cmd, 256, "sox -c 1 -r %d -e signed-integer -b 16 %s %s", (int)FE, RAW_OUT, FILE_OUT);
  system (cmd);
}

void
sound_file_write (double *s, FILE *fp)
{
  int i;
  short tmp[N];
  for (i=0; i<N; i++)
    {
      tmp[i] = (short)(s[i]*32768);
    }
  fwrite (tmp, sizeof(short), N, fp);
}
double freq_modif(double freq,double i) {
    return freq +(i*10);
}
int
main (int argc, char *argv[])
{
  int i;
  FILE *output;
  double s[N];

  if (argc != 1)
    {
      printf ("usage: %s\n", argv[0]);
      exit (EXIT_FAILURE);
    }

  output = sound_file_open_write ();
    int amp;
    double phi = 3.1416;//phase entre 0 et 2*pi
    double freq=440;//entre 22050 //440 La
    double freq2=880;
    double t ;
    //int t; t c'est i
    //son stationnaire
    // for (i=0; i<N; i++) {
    //     if(i%2==0) {
    //         s[i] = /*amp */sin(2.0*3.1416*freq*(double)i/(double)FE/*+phi*/); // ECHANTILLONS
    //     }else {
    //         s[i] = /*amp */sin(2.0*3.1416*freq2*(double)i/(double)FE/*+phi*/); // ECHANTILLONS
    //     }
    // }

    // for (i=0; i<N; i++) {
    // amp = i;
    //   if(i%2==0) {
    //        s[i] = amp *sin(2.0*3.1416*freq*(double)i/(double)FE/*+phi*/); // ECHANTILLONS
    //   }else {
    //        s[i] = amp *sin(2.0*3.1416*freq2*(double)i/(double)FE/*+phi*/); // ECHANTILLONS
    //   }
     
    // }

    //modif paramètre qui varie
    // for (i=0; i<N; i++) {
    //     t = i/FE;
    //   if(i%2==0) {
    //        //s[i] = /*amp */sin(2.0*3.1416*freq_modif(freq,i)*(double)i/(double)FE/*+phi*/)+1.0*sin(4*3.1416*t*freq); // ECHANTILLONS
    //        s[i] = sin(2.0*3.1416*freq*(double)i/(double)FE); // ECHANTILLONS
    //   }else {
    //        //s[i] = /*amp */sin(2.0*3.1416*freq_modif(freq2,i)*(double)i/(double)FE/*+phi*/)+1.0*sin(4*3.1416*t*freq2); // ECHANTILLONS
    //        s[i] = sin(2.0*3.1416*freq2*(double)i/(double)FE); // ECHANTILLONS
    //   }
    // }
    
      //synthèse additif
      for (i=0; i<N; i++) {
        //guitare électrique
        //s[i] = /*amp */sin(2.0*3.1416*117*(double)i/(double)FE/*+phi*/)+sin(2.0*3.1416*233*(double)i/(double)FE)+sin(2.0*3.1416*466*(double)i/(double)FE); // ECHANTILLONS

        s[i] = /*amp */cos(2.0*3.1416*117*(double)i/(double)FE/*+phi*/)+sin(2.0*3.1416*233*(double)i/(double)FE)+sin(2.0*3.1416*466*(double)i/(double)FE); // ECHANTILLONS
    
    }
  sound_file_write (s, output);
  sound_file_close_write (output);
  exit (EXIT_SUCCESS);
}
